﻿<?php session_start(); ?>

<?php 
	if (!isset($_SESSION['usuario'])) {
		header('Location: index.php');
	} 
?>


<?php include_once 'layout/header.php' ?>

	<ul id = "menu">
			<a href = "" target="_blank"><img id = "logo" src="Imagenes/Logo-UACJ.png"></a>
			<li><a href = "Mapa Curricular.php" class="active">Mapa Curricular</a></li>
			<li><a href = "Generar Horarios.php">Generar Horarios</a></li>
			<li><a href = "Reviews.php">Reviews</a></li>
			<li><a href = "helpers/logout.php">Log out</a></li>
	</ul>
	<br>
		
		<div class="w3-content w3-margin-top" style="max-width:1400px;">
			<!-- The Grid -->
			<div class="w3-row-padding">
			
			  <!-- Left Column -->
			  <div class="w3-third">
			  
				<div class="w3-white w3-text-grey w3-card-4">
				  <div class="w3-display-container">
					<img src="Imagenes/jordi.jpg" style="width:25%; margin-left: 37.5%;" alt="Avatar">
					
					  <h2 id="NombreAlumno" style="padding: 10px; text-align: center;"><?= $_SESSION['usuario']['nombre']; ?></h2>

				  </div>
				  <div class="w3-container">
					
					<div style="padding-left: 10%;" >
						<div style="float:left; width:50%;">
							<p>Matrícula:</p>
							<p>Programa:</p>
							<p>Estatus:</p>
							<p>Créditos cursados:</p>
						</div>
						<div style="float:left; width:50%;">
							<p id="matricula"><?= $_SESSION['usuario']['matricula']?></p>
							<p id="programa">Software</p>
							<p id="estatus">Activo</p>
							<p id="estatus"><?= $_SESSION['usuario']['creditos']; ?></p>
						</div>
					</div>
					
					
					<div class="w3-container">

						<?php
							include_once 'helpers/alumno.php';
							$alumno = new Alumno($_SESSION['usuario']['matricula']);
							$materiasEnCurso = $alumno->getMateriasEnCurso();
						?>
						<hr>
						<p class="w3-large"><b><i></i> Materias en curso</b></p>
						<ul class="w3-ul w3-border w3-hoverable" style="height:196px; overflow:hidden; overflow-y:scroll;">

						<?php foreach($materiasEnCurso as $materia): ?>
						  <li><?=$materia['materia']?></li>
						<?php endforeach; ?>

						</ul>
					</div>
					<br>
				  </div>
				</div><br>
		  
			  <!-- End Left Column -->
			  </div>
		  
			  <!-- Right Column -->
			  <div class="w3-twothird">
			  
				<div>
					<div id="obligatorias">
						<img src="Imagenes/mapa-obligatorias.png" style="width:100%;">
						
					</div>

					<div id="optativas" style="display: none">
						<img src="Imagenes/mapa-optativas.png" style="width:100%;">
						
					</div>
					<div style="margin-left: 76.5%;">
						<input id="btnObligatorias" type="button" class="w3-button w3-blue" value="Obligatorias" onclick="ocultar(false)">
						<input id="btnOptativas" type="button" class="w3-button w3-gray" value="Optativas" onclick="ocultar(true)">	
					</div>
				</div>
		  
			  <!-- End Right Column -->
			  </div>
			  
			<!-- End Grid -->
			</div>
			
			<!-- End Page Container -->
		  </div>

<?php include_once "layout/footer.php"?>
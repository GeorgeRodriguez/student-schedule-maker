﻿<?php session_start(); ?>
<?php 
	if (!isset($_SESSION['usuario'])) {
		header('Location: index.php');
	} 
	include_once 'layout/header.php';
	include_once 'helpers/review.php';

	$review = new Review();
?>


<ul id = "menu">
    <a href = "" target="_blank"><img id = "logo" src="Imagenes/Logo-UACJ.png"></a>
    <li><a href = "Mapa Curricular.php">Mapa Curricular</a></li>
    <li><a href = "Generar Horarios.php">Generar Horarios</a></li>
    <li><a class = "active" href = "Reviews.php">Reviews</a></li>
    <li><a href = "helpers/logout.php">Log out</a></li>
</ul>
		
<br>

		<div class="w3-content w3-margin-top" style="max-width:1400px;">

			<!-- The Grid -->
			<div class="w3-row-padding">
			
			  <!-- Left Column -->
			  <div class="w3-third" >
			  
			  
				<div class="w3-white w3-text-grey w3-card-4">
				<br>

				<form class="BusquedaInline" action="helpers/getReviews.php" method="POST">
				
					<input id="Profe" class="w3-input" type="text" name="nombreProfesor" placeholder="Nombre del profesor"><br>
					<input type="submit" name="submit" value="Buscar"  id="Buscar" class="w3-input">
					
					<?php if (isset($_SESSION['error'])): ?>
						<p><?= $_SESSION['error']; ?></p>

					<?php unset($_SESSION['error']); ?>
					<?php endif; ?>
				</form>
				<br>

				  <p class="w3-large w3-text-theme"><b><i class="fas fa-book"></i> Tus profesores</b></p>
					
				  <div class="w3-light-grey w3-round-xlarge">
					<div class="w3-round-xlarge w3-indigo w3-center">
						<?php
							include_once 'helpers/alumno.php';
							$alumno = new Alumno($_SESSION['usuario']['matricula']);
							$profesores = $alumno->getProfesores();
						?>
						<?php foreach ($profesores as $profesor): ?>
							<a class="link_profesor" href="helpers/getReviews.php?nombreProfesor=<?= $profesor['profesor'] ?>"><?=$profesor['profesor']?></a>
						<?php endforeach; ?>
					</div>
					<br>
				  </div> 
				</div> <br>
		  
			  <!-- End Left Column -->
			  </div>
		  
			  <!-- Right Column -->
			  <div class="w3-twothird">
				  
			<div style="height: 500px; overflow:hidden; overflow-y:scroll;">	
				
				<?php if (isset($_SESSION['reviews'])): ?>

				<?php $promedioCuenta = $review->getPromedioCuenta($_SESSION['reviews'][0]['profesor']); ?>
				<div>
					<span style="font-size: 30px">Profesor: <?=$_SESSION['reviews'][0]['profesor']?></span>
					<div>	
						<?php $calificacion = intval($promedioCuenta['promedio']);?>
						<?php for($i = 0; $i < 5; $i++): ?>
							<?php if ($calificacion > 0): ?>
								<span class="fa fa-star checked"></span>
							<?php else: ?>
								<span class="fa fa-star"></span>
							<?php endif; ?>
							<?php $calificacion = $calificacion - 1; ?>
						<?php endfor; ?>
						<div>
							<p><?= round($promedioCuenta['promedio'], 2); ?> de calificación promedio en <?= $promedioCuenta['cuenta']; ?> reseñas.</p>
						</div>
						
					</div>
				</div>
					
					


					<?php foreach ($_SESSION['reviews'] as $review): ?>
						<div class="testimonios">
							<img src="Imagenes/Simio.jpg" alt="Avatar" style="width:90px">
							<div class="w3-right">

								<?php $calificacion = intval($review['review_cal']);?>
								<?php for($i = 0; $i < 5; $i++): ?>
									<?php if ($calificacion > 0): ?>
										<span class="fa fa-star checked"></span>
									<?php else: ?>
										<span class="fa fa-star"></span>
									<?php endif; ?>
									<?php $calificacion = $calificacion - 1; ?>
								<?php endfor; ?>
							</div>
							<p><span><?= $review['alumno']; ?></span>Materia: <?= $review['materia']; ?></p>
							<p><?= $review['review_text']; ?></p>

						</div>
					<?php endforeach; ?>

				<br>
				<div>
				<?php
					$review = new Review();
					$materiasProfesor = $review->getMateriasProfesor($_SESSION['usuario']['matricula'] ,$_SESSION['reviews'][0]['id_profesor']);
				?>
				<?php if($materiasProfesor): ?>
					<fieldset>
						<form action="helpers/agregarReview.php" method="POST">

							<div>
									<select class="materias" name="materias" id="materias">
										<?php foreach($materiasProfesor as $materia): ?>
											<option value="<?= $materia['curso']; ?>"><?= $materia['nombre'] ?></option>
										<?php endforeach; ?>
									</select>
									<!--<textarea rows="4" cols="95" name="comment" placeholder="Ingrese un comentario..."></textarea>-->
									<textarea class='comment' name="comment" placeholder="Ingrese un comentario..."></textarea>
							</div>

							<div>
								<div class="rating_container">
									<div class="rating">
										<input type="radio"	name="star5"	id="star1"><label for="star1"></label>
										<input type="radio"	name="star4"	id="star2"><label for="star2"></label>
										<input type="radio"	name="star3"	id="star3"><label for="star3"></label>
										<input type="radio"	name="star2"	id="star4"><label for="star4"></label>
										<input type="radio"	name="star1"	id="star5"><label for="star5"></label>
									</div>
								</div>												
							</div>
							
							<div>
								<input type="submit" name="submit" id="Buscar" class="w3-button w3-blue w3-left" >
							</div>

							<input type="hidden" name="matricula" value="<?= $_SESSION['usuario']['matricula'] ?>">
							<input type="hidden" name="nombreProfesor" value="<?=$_SESSION['reviews'][0]['profesor']?>">

						

				</div>
						</form>
					</fieldset>
				<?php endif; ?>
				</div>
				
			<?php endif; ?>
			  <!-- End Right Column -->
			  </div>
			  
			<!-- End Grid -->
			</div>
			
			<!-- End Page Container -->
		  </div>
		
<?php include_once "layout/footer.php"?>
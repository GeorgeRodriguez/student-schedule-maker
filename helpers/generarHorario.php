<?php

session_start();

if(isset($_POST['submit'])) {

    include_once 'alumno.php';

    $numDeMaterias = intval($_POST['option']);

    $alumno = new Alumno($_SESSION['usuario']['matricula'], $_SESSION['usuario']['correo']);
    $horarioSugerido = $alumno->crearHorario($numDeMaterias);
    //var_dump($horarioSugerido);

    $_SESSION['horarioSugerido'] = $horarioSugerido;
}

header('Location: ../Generar Horarios.php');
?>

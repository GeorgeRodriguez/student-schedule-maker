<?php
include_once "conexion.php";

class Materia {

    public function getAll() {
        
        $conn = Connection::getConnection();
        $sql = "SELECT * FROM materias";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->get_result();
        $data = $result->fetch_all(MYSQLI_ASSOC);

        $stmt->close();
        $conn->close();
        return $data;
    }
}

?>
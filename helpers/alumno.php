<?php
include_once "conexion.php";

class Alumno {

    private $matricula;
    private $password;
    private $email;

    public function __construct($matricula = "", $email = "", $password = "") {
        $this->matricula = $matricula;
        $this->email = $email;
        $this->password = $password;
    }

    public function login() {
        $loginExitoso = false; // Los datos que se obtendran de la BD

        $conn = Connection::getConnection();
        $sql = "SELECT * FROM alumnos WHERE correo = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $this->email);
        $stmt->execute();

        // Obener resultado de la consulta
        $resultado = $stmt->get_result();
        if ($resultado->num_rows > 0) {

            $registro = $resultado->fetch_assoc();
            //$verify = password_verify($password, $registro['pass']);
            $passwordReal = $registro['password'];

            if ($this->password == $passwordReal) {
                
                //$_SESSION['usuario'] = $registro;
                $loginExitoso = true;
                $credtiosCursados = $this->getCreditosCursados($registro['matricula']);
                $registro['creditos'] = $credtiosCursados;
                if (!$registro['creditos']) {
                    $registro['creditos'] = 0;
                }
            } 
        } 

        $stmt->close();
        $conn->close();

        if ($loginExitoso) {
            return $registro;
        }
        else {
            return $loginExitoso;
        }
    }

    public function getCreditosCursados($matricula) {

        $conn = Connection::getConnection();
        $sql = "SELECT SUM(Creditos) as creditos FROM alumnos_cursos al, cursos c, materias m
        WHERE
        al.id_alumno = ? AND
        al.id_curso = c.id AND
        c.id_materia = m.id AND
        al.status = 2";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $matricula);
        $stmt->execute();

        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        $stmt->close();
        $conn->close();
        return $data["creditos"]; // String
    }

    // Devuelve todas las materia disponibles, pero sin repetir
    public function getAllMateriasDisponibles() {

        $conn = Connection::getConnection();
        $sql = "SELECT c.id, c.id_profesor, c.id_materia, c.id_horario, m.nombre as materia, p.nombre as profesor, h.nombre as horario, m.semestre, m.subsecuente, m.limite_creditos as creditos
        FROM cursos c, materias m, profesores p, horarios h 
        WHERE 
        c.id_materia = m.id AND 
        c.id_profesor = p.matricula AND
        c.id_horario = h.id AND 
        c.id_materia NOT IN ( SELECT id_materia FROM alumnos_cursos al, cursos c WHERE al.id_alumno = ? AND al.id_curso = c.id ) 
        GROUP BY c.id_materia 
        ORDER BY m.semestre ASC";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $this->matricula);
        $stmt->execute();

        $result = $stmt->get_result();
        $data = $result->fetch_all(MYSQLI_ASSOC);

        $stmt->close();
        $conn->close();
        return $data;
    }

    // Devuelve todas las materias disponibles, pero con repeticion
    public function getAllCursosDisponibles() {

        $conn = Connection::getConnection();
        $sql = "SELECT c.id, c.id_profesor, c.id_materia, c.id_horario, m.nombre as materia, p.nombre as profesor, h.nombre as horario, m.semestre, m.subsecuente, m.limite_creditos as creditos
        FROM cursos c, materias m, profesores p, horarios h 
        WHERE 
        c.id_materia = m.id AND 
        c.id_profesor = p.matricula AND
        c.id_horario = h.id AND 
        c.id_materia NOT IN ( SELECT id_materia FROM alumnos_cursos al, cursos c WHERE al.id_alumno = ? AND al.id_curso = c.id ) 
        ORDER BY m.semestre ASC";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $this->matricula);
        $stmt->execute();

        $result = $stmt->get_result();
        $data = $result->fetch_all(MYSQLI_ASSOC);

        $stmt->close();
        $conn->close();
        return $data;
    }

    public function crearHorario($numMaterias) {

        $materiasDisponibles = $this->getAllMateriasDisponibles();
        $materiasUsables = $this->limpiarCursos($materiasDisponibles);
        $horario = [];
        for($i = 0; $i < $numMaterias; $i++) {
            $horario[$i] = $materiasUsables[$i];
        }
        return $horario;
    }

    // Verifica si el alumno es apto para tomar los curos
    // Es decir, si ha tomado las materias subsecuentes y tiene el limite de creditos
    public function limpiarCursos($listaDeCursos) {

        $creditosObtenidos = $this->getCreditosCursados($this->matricula);

        $nuevaLista = [];
        foreach($listaDeCursos as $curso) {

            if (intval($curso['subsecuente']) != 0) {

                $alumnoEsApto = $this->llevoSubsecuente($this->matricula, $curso['subsecuente']);
                if ($alumnoEsApto && $creditosObtenidos >= $curso['creditos']) {
                    $nuevaLista[] = $curso;
                }
            }
            elseif ($creditosObtenidos >= $curso['creditos']) {
                $nuevaLista[] = $curso;
            }
        }
        return $nuevaLista;
    }

    // Comprobar si se llevo el subsecuente de una materia dada
    public function llevoSubsecuente($idAlumno, $idMateria) {

        $alumnoEsApto = false;

        $conn = Connection::getConnection();
        $sql = "SELECT al.id_curso FROM alumnos_cursos al, cursos c WHERE 
        al.id_curso = c.id AND
        al.id_alumno = ? AND
        c.id_materia = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("ii", $idAlumno, $idMateria);
        $stmt->execute();

        $result = $stmt->get_result();
        $data = $result->fetch_assoc();
        
        if ($data['id_curso']) {
            $alumnoEsApto = True;
        }

        $stmt->close();
        $conn->close();
        return $alumnoEsApto;
    }

    public function getProfesores() {

        $conn = Connection::getConnection();
        $sql = "SELECT al.id_curso, p.nombre as profesor FROM alumnos_cursos al, cursos c, profesores p
        WHERE 
        al.id_curso = c.id AND
        c.id_profesor = p.matricula AND
        al.id_alumno = ?
        GROUP BY p.nombre";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $this->matricula);
        $stmt->execute();

        $result = $stmt->get_result();
        $data = $result->fetch_all(MYSQLI_ASSOC);

        $stmt->close();
        $conn->close();
        return $data;
    }

    public function getMateriasEnCurso() {

        $conn = Connection::getConnection();
        $sql = "SELECT al.id_curso, m.nombre as materia FROM alumnos_cursos al, cursos c, materias m
        WHERE
        al.id_curso = c.id AND
        c.id_materia = m.id AND
        al.id_alumno = ? AND
        al.status = 1";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $this->matricula);
        $stmt->execute();

        $result = $stmt->get_result();
        $data = $result->fetch_all(MYSQLI_ASSOC);

        $stmt->close();
        $conn->close();
        return $data;
    }


}

?>
<?php

class Connection {

    public static function getConnection() {

        $host = 'localhost';
        $user = 'root';
        $pass = '';
        $db = 'ssm1';

        $conn = new mysqli($host, $user, $pass, $db);
        
        if ($conn->connect_errno) {
            return "Fallo en la conexion de BD";
        }
        $conn->set_charset('utf8');

        return $conn;
    }
}

?>
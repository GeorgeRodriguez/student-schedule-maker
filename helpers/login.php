<?php

    session_start();

    if (isset($_POST['submit'])) {
        include_once 'alumno.php';
        
        $email = trim($_POST['email']);
        $password = $_POST['password'];

        $alumno = new Alumno("", $email, $password);
        $loginData = $alumno->login(); // Recupera los datos del usuario

        if ($loginData) {
            $_SESSION['usuario'] = $loginData;
            header('Location: ../Mapa Curricular.php');
        }
        else { 
            $_SESSION['error'] = 'Usuario o contraseña incorrectos';
            header('Location: ../index.php');
        }
    } 

?>
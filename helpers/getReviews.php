<?php

session_start();

if(isset($_POST['submit']) || isset($_GET['nombreProfesor'])) {

    include_once 'review.php';

    if (isset($_POST['submit'])) {
        $nombreProfesor = $_POST['nombreProfesor'];
    }
    else {
        $nombreProfesor = $_GET['nombreProfesor'];
    }


    $review = new Review();
    $listaReviews = $review->getAllReviews($nombreProfesor);

    if ($listaReviews) {
        $_SESSION['reviews'] = $listaReviews;
    }
    else {
        $_SESSION['error'] = "No hay reviews del profesor seleccionado";
    }

    //var_dump($listaReviews);
}
header('Location: ../Reviews.php');


?>

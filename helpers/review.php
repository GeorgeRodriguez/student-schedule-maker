<?php

include_once 'conexion.php';

class Review {

    public function getAllReviews($nombreProfesor) {

        $conn = Connection::getConnection();
        $sql = "SELECT al.id_alumno, al.id_curso, al.review_cal, al.review_text, c.id_profesor, p.nombre as profesor, m.nombre as materia, a.nombre as alumno FROM alumnos_cursos al, cursos c, profesores p, materias m, alumnos a
        WHERE
        al.id_curso = c.id AND
        c.id_materia = m.id AND
        c.id_profesor = p.matricula AND
        al.id_alumno = a.matricula AND
        p.nombre = ?
        HAVING al.review_text is NOT null";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $nombreProfesor);
        $stmt->execute();

        $result = $stmt->get_result();
        $data = $result->fetch_all(MYSQLI_ASSOC);

        $stmt->close();
        $conn->close();
        return $data;
    }

    public function getPromedioCuenta($nombreProfesor) {

        
        $conn = Connection::getConnection();
        $sql = "SELECT AVG(al.review_cal) as promedio, COUNT(al.review_cal) as cuenta FROM alumnos_cursos al, cursos c, profesores p, materias m, alumnos a
        WHERE
        al.id_curso = c.id AND
        c.id_materia = m.id AND
        c.id_profesor = p.matricula AND
        al.id_alumno = a.matricula AND
        p.nombre = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $nombreProfesor);
        $stmt->execute();

        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        $stmt->close();
        $conn->close();
        return $data;
    }

    public function getMateriasProfesor($idAlumno, $idProfesor) {

        $conn = Connection::getConnection();
        $sql = "SELECT c.id_materia, m.nombre, c.id as curso FROM cursos c, materias m, alumnos_cursos al
        WHERE 
        al.id_curso = c.id AND
        al.id_alumno = ? AND
        c.id_materia = m.id AND
        id_profesor = ?
        GROUP BY c.id_materia";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("ii", $idAlumno, $idProfesor);
        $stmt->execute();

        $result = $stmt->get_result();
        $data = $result->fetch_all(MYSQLI_ASSOC);

        $stmt->close();
        $conn->close();
        return $data;
    }

    public function agregarReview($idAlumno, $idCurso, $reviewCalif, $reviewText) {

        $exito = false;

        $conn = Connection::getConnection();
        $sql = "UPDATE alumnos_cursos SET review_cal = ?, review_text = ?
        WHERE
        id_alumno = ? AND 
        id_curso = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("isii", $reviewCalif, $reviewText, $idAlumno, $idCurso);
        $stmt->execute();

        if ($stmt->affected_rows == 1) {
            $exito = true;
        }

        $stmt->close();
        $conn->close();
        return $exito;
    }
}

?>
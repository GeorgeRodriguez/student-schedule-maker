<?php session_start(); ?>

<?php include_once "layout/header.php" ?>

    <ul id = "menu">
			<a href = "" target="_blank"><img id = "logo" src="Imagenes/Logo-UACJ.png"></a>
	</ul>
    <br>
    <br>

    <form class="form_login" action="helpers/login.php" method="POST">
        <h1 class="form_login_title">SSM Login</h1>
        <label for="correo">Correo:</label>
        <input type="email" name="email" id="correo">
        <label for="password">Contraseña:</label>
        <input type="password" name="password" id="password">
        <input type="submit" name="submit" value="Entrar">

        <?php if (isset($_SESSION['error'])): ?>
        <p class="error">Usuario o contraseña incorrectos</p>
        <?php session_destroy(); ?>
        <?php endif; ?>
    </form>

<?php include_once "layout/footer.php"; ?>
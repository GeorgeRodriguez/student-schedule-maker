-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 08-11-2020 a las 20:46:14
-- Versión del servidor: 5.7.31
-- Versión de PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ssm1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

DROP TABLE IF EXISTS `alumnos`;
CREATE TABLE IF NOT EXISTS `alumnos` (
  `matricula` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`matricula`, `nombre`, `correo`, `password`) VALUES
(100001, 'Alexis Santillan', '100001@alumnos.uacj.mx', '123'),
(100002, 'Ulises Hernandez', '100002@alumnos.uacj.mx', '123'),
(100003, 'Roberto Mares', '100003@alumnos.uacj.mx', '123'),
(100004, 'Armin Alvidrez', '100004@alumnos.uacj.mx', '123'),
(100005, 'George Rodriguez', '100005@alumnos.uacj.mx', '123'),
(100006, 'Luis Valdez', '100006@alumnos.uacj.mx', '123'),
(100007, 'Kaleb Chavira', '100007@alumnos.uacj.mx', '123'),
(100008, 'Martin Vera', '100008@alumnos.uacj.mx', '123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos_cursos`
--

DROP TABLE IF EXISTS `alumnos_cursos`;
CREATE TABLE IF NOT EXISTS `alumnos_cursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_alumno` int(11) DEFAULT NULL,
  `id_curso` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `review_cal` int(11) DEFAULT NULL,
  `review_text` text,
  PRIMARY KEY (`id`),
  KEY `fk_alumno` (`id_alumno`),
  KEY `fk_curso` (`id_curso`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alumnos_cursos`
--

INSERT INTO `alumnos_cursos` (`id`, `id_alumno`, `id_curso`, `status`, `review_cal`, `review_text`) VALUES
(8, 100001, 8, 1, 5, 'Muy bueno\r\n'),
(9, 100001, 9, 1, 0, 'El profesor es bueno\r\n'),
(10, 100001, 10, 1, 0, 'nvdnvfvfvfvrfrv'),
(11, 100001, 11, 1, 3, 'La clase esta normal'),
(12, 100001, 12, 1, NULL, NULL),
(13, 100002, 7, 2, NULL, NULL),
(14, 100002, 8, 2, 4, 'No es muy bueno pero se entiende lo que explica'),
(15, 100002, 9, 2, NULL, NULL),
(16, 100002, 10, 2, 2, 'La clase es tranquila y facil de pasar'),
(17, 100002, 11, 2, NULL, NULL),
(18, 100002, 12, 2, NULL, NULL),
(19, 100002, 14, 1, NULL, NULL),
(20, 100002, 15, 1, NULL, NULL),
(21, 100002, 16, 1, 3, 'Materia aburrida, buen profe'),
(22, 100002, 17, 1, 3, 'No esta tan mal, explica bien los temas pero deja mucha tarea'),
(23, 100003, 31, 1, 4, 'Muy buena clase, se aprende mucho');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

DROP TABLE IF EXISTS `cursos`;
CREATE TABLE IF NOT EXISTS `cursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_profesor` int(11) DEFAULT NULL,
  `id_materia` int(11) DEFAULT NULL,
  `id_horario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_profesor` (`id_profesor`,`id_horario`),
  KEY `fk_materia` (`id_materia`),
  KEY `fk_horario` (`id_horario`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `id_profesor`, `id_materia`, `id_horario`) VALUES
(7, 1, 1, 1),
(8, 1, 2, 2),
(9, 1, 3, 3),
(10, 2, 4, 7),
(11, 3, 5, 8),
(12, 3, 6, 9),
(13, 8, 1, 4),
(14, 2, 7, 1),
(15, 2, 8, 2),
(16, 2, 9, 3),
(17, 1, 10, 7),
(18, 1, 11, 8),
(19, 1, 12, 9),
(20, 8, 10, 7),
(21, 8, 7, 10),
(22, 2, 8, 11),
(23, 3, 13, 1),
(24, 3, 14, 2),
(25, 3, 15, 3),
(26, 3, 16, 7),
(27, 2, 17, 8),
(28, 2, 18, 9),
(29, 7, 14, 1),
(30, 4, 19, 1),
(31, 4, 20, 2),
(32, 5, 21, 3),
(33, 5, 22, 7),
(34, 6, 23, 8),
(35, 6, 24, 9),
(36, 7, 24, 4),
(37, 7, 23, 5),
(38, 6, 25, 4),
(39, 6, 26, 5),
(40, 5, 27, 6),
(41, 5, 28, 10),
(42, 4, 29, 11),
(43, 4, 30, 12),
(44, 4, 31, 4),
(45, 4, 32, 5),
(46, 4, 33, 6),
(47, 6, 35, 10),
(48, 5, 35, 11),
(49, 5, 36, 12),
(50, 5, 37, 9),
(51, 4, 38, 10),
(52, 7, 39, 11),
(53, 7, 40, 12),
(54, 8, 41, 11),
(55, 1, 42, 12),
(56, 3, 43, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

DROP TABLE IF EXISTS `horarios`;
CREATE TABLE IF NOT EXISTS `horarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora_inicio` time DEFAULT NULL,
  `hora_fin` time DEFAULT NULL,
  `lun` bit(1) DEFAULT NULL,
  `mar` bit(1) DEFAULT NULL,
  `mie` bit(1) DEFAULT NULL,
  `jue` bit(1) DEFAULT NULL,
  `vie` bit(1) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `horarios`
--

INSERT INTO `horarios` (`id`, `hora_inicio`, `hora_fin`, `lun`, `mar`, `mie`, `jue`, `vie`, `nombre`) VALUES
(1, '08:00:00', '10:00:00', b'1', b'0', b'1', b'0', b'0', 'Lunes y Miercoles de 08:00 a 10:00'),
(2, '10:00:00', '12:00:00', b'1', b'0', b'1', b'0', b'0', 'Lunes y Miercoles de 10:00 a 12:00'),
(3, '12:00:00', '14:00:00', b'1', b'0', b'1', b'0', b'0', 'Lunes y Miercoles de 12:00 a 14:00'),
(4, '14:00:00', '16:00:00', b'1', b'0', b'1', b'0', b'0', 'Lunes y Miercoles de 14:00 a 16:00'),
(5, '16:00:00', '18:00:00', b'1', b'0', b'1', b'0', b'0', 'Lunes y Miercoles de 16:00 a 18:00'),
(6, '18:00:00', '20:00:00', b'1', b'0', b'1', b'0', b'0', 'Lunes y Miercoles de 18:00 a 20:00'),
(7, '08:00:00', '10:00:00', b'0', b'1', b'0', b'1', b'0', 'Martes y Jueves de 08:00 a 10:00'),
(8, '10:00:00', '12:00:00', b'0', b'1', b'0', b'1', b'0', 'Martes y Jueves de 10:00 a 12:00'),
(9, '12:00:00', '14:00:00', b'0', b'1', b'0', b'1', b'0', 'Martes y Jueves de 12:00 a 14:00'),
(10, '14:00:00', '16:00:00', b'0', b'1', b'0', b'1', b'0', 'Martes y Jueves de 14:00 a 16:00'),
(11, '16:00:00', '18:00:00', b'0', b'1', b'0', b'1', b'0', 'Martes y Jueves de 16:00 a 18:00'),
(12, '18:00:00', '20:00:00', b'0', b'1', b'0', b'1', b'0', 'Martes y Jueves de 18:00 a 20:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

DROP TABLE IF EXISTS `materias`;
CREATE TABLE IF NOT EXISTS `materias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `semestre` int(11) DEFAULT NULL,
  `creditos` int(11) DEFAULT NULL,
  `limite_creditos` int(11) DEFAULT NULL,
  `subsecuente` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`id`, `nombre`, `semestre`, `creditos`, `limite_creditos`, `subsecuente`) VALUES
(1, 'Calculo 1', 1, 8, 0, 0),
(2, 'Logica matematica', 1, 8, 0, 0),
(3, 'Algebra superior', 1, 8, 0, 0),
(4, 'Competencias comunicativas 1', 1, 8, 0, 0),
(5, 'Probabilidad', 1, 8, 0, 0),
(6, 'Fisica 1', 1, 8, 0, 0),
(7, 'Calculo 2', 2, 8, 0, 1),
(8, 'Algebra lineal', 2, 8, 0, 2),
(9, 'Matematicas discretas', 2, 8, 0, 0),
(10, 'Metodologia de la programacion', 2, 8, 0, 0),
(11, 'Introduccion a las computadoras', 2, 8, 0, 0),
(12, 'Informatica y sociedad', 2, 8, 0, 0),
(13, 'Estadistica', 3, 8, 0, 5),
(14, 'Analisis numerico', 3, 8, 0, 0),
(15, 'Interaccion hombre maquina', 3, 8, 0, 0),
(16, 'Teoria de lenguajes de programacion', 3, 8, 0, 0),
(17, 'Fundamentos de ingenieria de software', 3, 8, 0, 0),
(18, 'Programacion 1', 3, 8, 0, 10),
(19, 'Teoria de la computacion', 4, 8, 0, 0),
(20, 'Bases de datos 1', 4, 8, 0, 0),
(21, 'Desarrollo de requisitos de software', 4, 8, 0, 0),
(22, 'Competencias comunicativas 2', 4, 8, 0, 0),
(23, 'Estructura de datos', 4, 8, 0, 18),
(24, 'Programacion 2', 4, 8, 0, 18),
(25, 'Seguridad de la informacion 1', 5, 8, 0, 24),
(26, 'Arquitectura de computadoras', 5, 8, 0, 0),
(27, 'Arquitectura de software', 5, 8, 0, 21),
(28, 'Sistemas operativos', 5, 8, 0, 0),
(29, 'Metodos formales para el diseño de software', 5, 8, 0, 21),
(30, 'Servicio social', 5, 8, 200, 0),
(31, 'Seguridad de la informacion 2', 6, 8, 0, 25),
(32, 'Métricas de software', 6, 8, 238, 0),
(33, 'Investigacion aplicada', 6, 8, 0, 0),
(34, 'Administracion de proyectos', 6, 8, 0, 0),
(35, 'Aseguramiento de la calidad del software', 6, 8, 0, 0),
(36, 'Introduccion a la ingenieria del conocimiento', 6, 8, 0, 0),
(37, 'Sistemas distribuidos', 7, 8, 0, 0),
(38, 'Topicos selectos', 7, 8, 286, 0),
(39, 'Competencias comunicativas 3', 7, 8, 0, 0),
(40, 'Innovación tecnologica', 7, 8, 286, 0),
(41, 'Seminario de titulacion 1', 8, 8, 328, 0),
(42, 'Practicas profesionales', 8, 8, 200, 0),
(43, 'Seminario de titulacion 2', 9, 8, 352, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesores`
--

DROP TABLE IF EXISTS `profesores`;
CREATE TABLE IF NOT EXISTS `profesores` (
  `matricula` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`matricula`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `profesores`
--

INSERT INTO `profesores` (`matricula`, `nombre`) VALUES
(1, 'Roberto Sanchez'),
(2, 'Gabriel Solis'),
(3, 'Juan Perez'),
(4, 'Luis Rodriguez'),
(5, 'Hector Fernandez'),
(6, 'Antonio Antillon'),
(7, 'Carlos Ramirez'),
(8, 'Jorge Gonzalez');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumnos_cursos`
--
ALTER TABLE `alumnos_cursos`
  ADD CONSTRAINT `fk_alumno` FOREIGN KEY (`id_alumno`) REFERENCES `alumnos` (`matricula`),
  ADD CONSTRAINT `fk_curso` FOREIGN KEY (`id_curso`) REFERENCES `cursos` (`id`);

--
-- Filtros para la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD CONSTRAINT `fk_horario` FOREIGN KEY (`id_horario`) REFERENCES `horarios` (`id`),
  ADD CONSTRAINT `fk_materia` FOREIGN KEY (`id_materia`) REFERENCES `materias` (`id`),
  ADD CONSTRAINT `fk_profesor` FOREIGN KEY (`id_profesor`) REFERENCES `profesores` (`matricula`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<footer class="w3-container w3-indigo w3-center w3-margin-top">
	  <p>Universidad Autonoma de Ciudad Juarez</p>
	  <i class="fa fa-facebook-official w3-hover-opacity"></i>
	  <i class="fa fa-instagram w3-hover-opacity"></i>
	  <i class="fa fa-snapchat w3-hover-opacity"></i>
	  <i class="fa fa-pinterest-p w3-hover-opacity"></i>
	  <i class="fa fa-twitter w3-hover-opacity"></i>
	  <i class="fa fa-linkedin w3-hover-opacity"></i>
	</footer>
	
    </body>
    
    <script >
		function ocultar(y) {
			if(y){
				document.getElementById("obligatorias").style.display = "none";
				document.getElementById("btnObligatorias").classList.replace("w3-blue", "w3-gray");
				document.getElementById("btnOptativas").classList.replace("w3-gray", "w3-blue");
				document.getElementById("optativas").style.display = "initial";
			}else{
				document.getElementById("optativas").style.display = "none";
				document.getElementById("btnOptativas").classList.replace("w3-blue", "w3-gray");
				document.getElementById("btnObligatorias").classList.replace("w3-gray", "w3-blue");
				document.getElementById("obligatorias").style.display = "initial";
			}

		  }
	</script>
</html>
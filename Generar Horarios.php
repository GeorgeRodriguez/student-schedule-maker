﻿<?php session_start(); ?>

<?php 
	if (!isset($_SESSION['usuario'])) {
		header('Location: index.php');
	} 
	include_once 'layout/header.php';
	include_once 'helpers/alumno.php';
?>

	<ul id = "menu">
		<a href = "" target="_blank"><img id = "logo" src="Imagenes/Logo-UACJ.png"></a>
		<li><a href = "Mapa Curricular.php">Mapa Curricular</a></li>
		<li><a href = "Generar Horarios.php" class="active">Generar Horarios</a></li>
		<li><a href = "Reviews.php">Reviews</a></li>
		<li><a href = "helpers/logout.php">Log out</a></li>
	</ul>


	<!-- Page Container -->
	<div class="w3-content w3-margin-top" style="max-width:1400px;">
	
	  <!-- The Grid -->
	  <div class="w3-row-padding">
	  
		<!-- Left Column -->
		<div class="w3-third" style="height:800px; overflow:hidden; overflow-y:scroll;">
		
		  <div class="w3-white w3-text-grey w3-card-4">

			<div class="w3-display-container">
			  <img src="Imagenes/jordi.jpg" style="width:100%" alt="Avatar">
			  <div class="w3-display-bottomleft w3-container w3-text-black">
				<h2><?= $_SESSION['usuario']['nombre']; ?></h2>
			  </div>
			</div>

			<div class="w3-container w3-responsive">
			  <p>Matricula: <?= $_SESSION['usuario']['matricula']; ?></p>
			  <p>Programa: Ing. de Software</p>
			  <p>Estatus: Inscrito</p>
			  <p>Creditos Programa: 376</p>
			  <p>Creditos Cursados: <?= $_SESSION['usuario']['creditos']; ?></p>
			  <hr>
			  <p class="w3-large"><b><i class="fas fa-toolbox"></i> Preferencias</b></p>

			  <form action="helpers/generarHorario.php" method="POST"> 

			  <p>¿Cuantas materias deseas cursar?</p>
			  <div class="w3-round-xlarge w3-small" style="background-color: #1177d1;">
				<div class="w3-container w3-center w3-round-xlarge"> 
					<div class="w3-center" >
						<select class="w3-select w3-border-0 w3-round w3-text-white" name="option" style="background-color: #1177d1;">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
						  </select>
					</div>
				</div>
			  </div> 
			<br> <br>
			<p></p>
			<div class="w3-light-grey w3-round-xlarge w3-small">
				<div class="w3-container w3-center w3-round-xlarge" style="background-color: #990000">
					<input type="submit" class="w3-bar-item w3-button w3-text-white" name="submit" value="Generar" style="width: 90%; background-color: #990000;">
				</div>
			</div>

			</form>

			<br> <br>
			  <p class="w3-large w3-text-theme"><b><i class="fas fa-book"></i> 
			  Materias disponibles</b></p>

				<div class="w3-center">
					<table  class="w3-text-white w3-table w3-bordered w3-centered" style="width: 100%; background-color: #1177d1;">
						<tr>
						  <th>Materia</th>
						  <th>Horario</th>
						  <th>Profesor</th>
						  <th>Grupo</th>
						  <th></th>
						</tr>
						
						<?php 
						$alumno = new Alumno($_SESSION['usuario']['matricula']);
						$materiasDisponibles = $alumno->getAllCursosDisponibles();
						$materiasUsables = $alumno->limpiarCursos($materiasDisponibles);
						?>
						<?php foreach($materiasUsables as $materia): ?>
							<tr>
								<td><?= $materia['materia'] ?></td>
								<td><?= $materia['horario'] ?></td>
								<td><?= $materia['profesor'] ?></td>
								<td><?= $materia['id'] ?></td>
								<td><button onclick="add(this)">Add</button></td>
							</tr>
						<?php endforeach;?>

					  </table>
				</div>
			  <br>
			</div>
		  </div><br>
	
		<!-- End Left Column -->
		</div>
	
		
		<!-- Right Column -->
		<div class="w3-twothird ">
			<h2>Horario Seleccionado</h2>
			<?php if (isset($_SESSION['horarioSugerido'])): ?>

			<div class="w3-center w3-responsive">
				<table id="tabla_horario" class="w3-text-white w3-table w3-tiny w3-bordered w3-centered" style="width: 100%; background-color: #1177d1;">
					<tr>
					  	<th>Materia</th>
					  	<th>Horario</th>
						<th>Profesor</th>
						<th>Grupo</th>
						<th></th>
					</tr>

					<?php foreach($_SESSION['horarioSugerido'] as $materia): ?>
						<tr>
							<td><?= $materia['materia'] ?></td>
							<td><?= $materia['horario'] ?></td>
							<td><?= $materia['profesor'] ?></td>
							<td><?= $materia['id'] ?></td>
							<td><button onclick="quit(this)">Quit</button></td>
						</tr>
					<?php endforeach; ?>
				</table>
				</div>
		
		<!-- End Right Column -->
		</div>

		<?php endif; ?>
		
	  <!-- End Grid -->
	  </div>
	  
	  <!-- End Page Container -->
	</div>
	
	<script>
		function quit(b) {
			const row = b.parentElement.parentElement;
			const rowParent = row.parentElement;
			console.log(rowParent);
			rowParent.removeChild(row);
		}

		function add(b) {
			let row = b.parentElement.parentElement;
			let rowNueva = row.cloneNode(true);
			let buttonNuevo = rowNueva.lastElementChild.lastElementChild;
			buttonNuevo.setAttribute('onclick', 'quit(this)');
			buttonNuevo.textContent = "Quit";

			let tabla = document.getElementById('tabla_horario');
			tabla.appendChild(rowNueva);
			console.log(rowNueva);
			
		}
	</script>
<?php include_once "layout/footer.php"?>